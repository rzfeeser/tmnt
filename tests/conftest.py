import pytest
from model.tmnt import app   # this creates an instance of our flask app

@pytest.fixture()
def client():
    yield app.test_client()
